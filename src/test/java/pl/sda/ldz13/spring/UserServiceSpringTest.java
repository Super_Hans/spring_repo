package pl.sda.ldz13.spring;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import pl.sda.ldz13.spring.config.AppConfiguration;
import pl.sda.ldz13.spring.userService.UserServiceSingleton;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {AppConfiguration.class})
public class UserServiceSpringTest {

    @Autowired
    private UserServiceSingleton userServiceAnnotationSingleton;

    @Test
    public void addUser() {
        userServiceAnnotationSingleton.addUserToPrototype("Test", 21);
        Assert.assertEquals("Test", userServiceAnnotationSingleton.getUserPrototype("Test").getName());
        Assert.assertEquals(21, userServiceAnnotationSingleton.getUserPrototype("Test").getAge());
    }
}
