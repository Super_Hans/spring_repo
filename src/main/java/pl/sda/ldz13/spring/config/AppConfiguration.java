package pl.sda.ldz13.spring.config;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import pl.sda.ldz13.spring.userRepository.UserRepo;
import pl.sda.ldz13.spring.userRepository.UserRepoPrototype;
import pl.sda.ldz13.spring.userRepository.UserRepoSingleton;

@org.springframework.context.annotation.Configuration
@ComponentScan("pl.sda.ldz13.spring")
public class AppConfiguration {

    @Bean(initMethod = "init", destroyMethod = "destroy")
    public UserRepo userRepoSingletonBean() {
        return new UserRepoSingleton();
    }

    @Bean(initMethod = "init", destroyMethod = "destroy")
    @Scope(BeanDefinition.SCOPE_PROTOTYPE) //("prototype")
    public UserRepo userRepoPrototypeBean() {
        return new UserRepoPrototype();
    }

}
