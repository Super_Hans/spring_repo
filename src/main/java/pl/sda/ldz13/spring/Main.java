package pl.sda.ldz13.spring;

import com.sun.xml.internal.bind.v2.TODO;
import org.apache.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.sda.ldz13.spring.config.AppConfiguration;
import pl.sda.ldz13.spring.model.User;
import pl.sda.ldz13.spring.userRepository.UserRepo;
import pl.sda.ldz13.spring.userService.UserService;
import pl.sda.ldz13.spring.userService.UserServicePrototype;
import pl.sda.ldz13.spring.userService.UserServiceSingleton;

public class Main {
    private static Logger log = Logger.getLogger(Main.class);


    public static void main(String[] args) {
        log.info("Hello!");
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(AppConfiguration.class);
        context.refresh();
//       getUserFromUserServicePrototype(context);
//       showUserFromService(context);
//       excercise3(context);
        exercise4(context);

        context.close();
    }

    private static void getUserFromUserServicePrototype(AnnotationConfigApplicationContext context) {
        log.info("-----------info----------");
        UserServicePrototype prototype = (UserServicePrototype) context.getBean("userServicePrototype");
        prototype.addUserToPrototype("Tomek", 88);
        prototype.addUserToSingleton("Mateusz", 17);

        log.info("UserServicePrototype-->" + prototype.getUserPrototype("Tomek").getName());
        log.info("UserServiceSingleton-->" + prototype.getUserSingleton("Mateusz").getName());
    }

    private static void showUserFromService(AnnotationConfigApplicationContext context) {
        log.info("==========info2========");
        UserRepo userRepoAnnotationSingleton = (UserRepo) context.getBean("userRepoAnnotationSingleton");
        User user = userRepoAnnotationSingleton.getUserByName("Mateusz");
        log.info("userRepoAnnotationSingleton getUserByName: " + user.getName());

        try {
            UserRepo userRepoAnnotationPrototype = (UserRepo) context.getBean("userRepoAnnotationPrototype");
            //User user1 = userRepoAnnotationPrototype.getUserByName("Tomek");
            log.info("UserRepoAnnotationPrototype getUserByName: " + userRepoAnnotationPrototype.getUserByName("Tomek").getName());
        }
        catch (Exception e){
            log.error("User not found");
        }

    }


    private static void excercise3(AnnotationConfigApplicationContext context){
        UserServicePrototype userServicePrototype = (UserServicePrototype)context.getBean("userServicePrototype");
        userServicePrototype.addUserToPrototype("Mariusz",22);
        userServicePrototype.addUserToSingleton("Ziemowit",32);

        log.info("UserServicePrototype getUserNumberFromPrototype: " + userServicePrototype.getUsersNumberFromPrototype());
        log.info("UserServicePrototype getUserNumberFromSingleton: " + userServicePrototype.getUsersNumberFromSingleton());
    }
    private static void exercise4(AnnotationConfigApplicationContext context) {
        log.info("--------------exercise4---------------------------------");
        UserServiceSingleton userServiceSingleton = (UserServiceSingleton) context.getBean("userServiceSingleton");
        userServiceSingleton.addUserToPrototype("Tomek", 88);
        userServiceSingleton.addUserToSingleton("Mateusz", 17);

        userServiceSingleton = (UserServiceSingleton) context.getBean("userServiceSingleton");
        userServiceSingleton.addUserToPrototype("Piotr", 41);
        userServiceSingleton.addUserToSingleton("Marian", 35);

        log.info("userServiceSingleton getUsersNumberFromPrototype: " + userServiceSingleton.getUsersNumberFromPrototype());
        log.info("userServiceSingleton getUsersNumberFromSingleton: " + userServiceSingleton.getUsersNumberFromSingleton());
    }
}
