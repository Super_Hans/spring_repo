package pl.sda.ldz13.spring.model;

public class User {
    private Long id;
    private String name;
    private int age;

    public User(long id, String name, int age) {
        this.name = name;
        this.id = id;
        this.age = age;
    }


    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
