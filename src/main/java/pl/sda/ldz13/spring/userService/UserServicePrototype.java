package pl.sda.ldz13.spring.userService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import pl.sda.ldz13.spring.model.User;
import pl.sda.ldz13.spring.userRepository.UserRepo;

@Service
@Scope("prototype")
public class UserServicePrototype implements UserService {

    @Autowired
    @Qualifier("userRepoAnnotationSingleton")
    private UserRepo userRepoSingleton;

    @Autowired
    @Qualifier("userRepoAnnotationPrototype")
    private UserRepo userRepoPrototype;

    @Override
    public void addUserToPrototype(String name, int age) {
        userRepoPrototype.addUser(name, age);

    }

    @Override
    public void addUserToSingleton(String name, int age) {
        userRepoSingleton.addUser(name, age);

    }

    @Override
    public User getUserPrototype(String name) {
        return userRepoPrototype.getUserByName(name);
    }

    @Override
    public User getUserSingleton(String name) {
        return userRepoSingleton.getUserByName(name);
    }

    @Override
    public int getUsersNumberFromSingleton() {
        return userRepoSingleton.getAllUsers().size();
    }

    @Override
    public int getUsersNumberFromPrototype() {
        return userRepoPrototype.getAllUsers().size();
    }
}
