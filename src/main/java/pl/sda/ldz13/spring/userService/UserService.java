package pl.sda.ldz13.spring.userService;

import pl.sda.ldz13.spring.model.User;

public interface UserService {
    void addUserToPrototype(String name, int age);

    void addUserToSingleton(String name, int age);

    User getUserPrototype(String name);

    User getUserSingleton(String name);

    int getUsersNumberFromSingleton();

    int getUsersNumberFromPrototype();

}
