package pl.sda.ldz13.spring.userRepository;

import org.apache.log4j.Logger;
import pl.sda.ldz13.spring.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepoPrototype implements UserRepo {
    private Logger log = Logger.getLogger(UserRepoPrototype.class);
    private List<User> users;

    @Override
    public Long addUser(String name, int age) {

        long id = System.nanoTime();
        users.add(new User(id, name, age));
        return id;
    }

    @Override
    public User getUserById(Long id) {
        return users.stream()
                .filter(user -> user.getId().equals(id))
                .findAny()
                .orElseThrow(() -> new NullPointerException("User not found"));
    }

    @Override
    public User getUserByName(String name) {
        return users.stream()
                .filter(user -> user.getName().equals(name))
                .findAny()
                .orElseThrow(() -> new NullPointerException("User not found"));
    }

    @Override
    public List<User> getAllUsers() {
        return users;
    }

    @Override
    public void init() {
        log.info("init UserRepoPrototype");
        users = new ArrayList<>();
    }

    @Override
    public void destroy() {
        log.info("Object has been destroyed");
    }
}
