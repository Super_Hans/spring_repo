package pl.sda.ldz13.spring.userRepository;

import pl.sda.ldz13.spring.model.User;

import java.util.List;

public interface UserRepo {

    Long addUser(String name, int age);

    User getUserById(Long id);

    User getUserByName(String name);

    List<User> getAllUsers();

    void init();

    void destroy();

}
