package pl.sda.ldz13.spring.userRepository;

import org.apache.log4j.Logger;
import pl.sda.ldz13.spring.model.User;

import java.util.ArrayList;
import java.util.List;

public class UserRepoSingleton implements UserRepo {
    private Logger log = Logger.getLogger(UserRepoSingleton.class);
    private List<User> users;

    @Override
    public Long addUser(String name, int age) {
        User user = new User(addUser(name,age), name, age);
        user.setId(System.nanoTime());
        users.add(user);
        return user.getId();
    }

    @Override
    public User getUserById(Long id) {
        return null;
    }

    @Override
    public User getUserByName(String name) {
        return null;
    }

    @Override
    public List<User> getAllUsers() {
        return users;
    }

    @Override
    public void init() {
        log.info("init UserRepoSingleton");
        users = new ArrayList<>();

    }

    @Override
    public void destroy() {
        log.info("Object has been destroyed");
    }
}
