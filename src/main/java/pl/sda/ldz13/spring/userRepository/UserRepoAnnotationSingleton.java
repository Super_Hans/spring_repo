package pl.sda.ldz13.spring.userRepository;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import pl.sda.ldz13.spring.model.User;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UserRepoAnnotationSingleton implements UserRepo {
    private Logger log = Logger.getLogger(UserRepoAnnotationSingleton.class);
    private List<User> users = null;

    @Override
    public Long addUser(String name, int age) {

        long id = System.nanoTime();
        users.add(new User(id, name, age));
        return id;
    }

    @Override
    public User getUserById(Long id) {
        return users.stream()
                .filter(user -> user.getId().equals(id))
                .findAny()
                .orElseThrow(() -> new NullPointerException("User not found"));
    }

    @Override
    public User getUserByName(String name) {
        return users.stream()
                .filter(user -> user.getName().equals(name))
                .findAny()
                .orElseThrow(() -> new NullPointerException("User not found"));
    }

    @Override
    public List<User> getAllUsers() {
        return users;
    }


    @PostConstruct
    @Override
    public void init() {
        log.info("init UserRepoPrototype");
        users = new ArrayList<>();
    }

    @PreDestroy
    @Override
    public void destroy() {
        log.info("Object has been destroyed");
    }
}